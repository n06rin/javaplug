package com.n06rin.gliders;
 
import android.content.Context;
import android.content.ContentValues;
import android.provider.MediaStore.Images;
import android.provider.MediaStore;
import android.widget.Toast;

import android.content.Intent;



import android.app.Activity;
import com.unity3d.player.UnityPlayerActivity;


import com.unity3d.player.UnityPlayer;

import android.os.Bundle;
import android.util.Log;

import android.os.Build;


 
public class WorkingWithGallery extends UnityPlayerActivity{
    
    

    static final int PICK_IMAGE_REQUEST = 1;


    protected void onCreate(Bundle savedInstanceState) {

    // call UnityPlayerActivity.onCreate()
    super.onCreate(savedInstanceState);

    // print debug message to logcat
    Log.d("OverrideActivity", "onCreate called!");
  }



    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }


            String realPath;


            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
            
            // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
            
            // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            UnityPlayer.UnitySendMessage("folder", "loadImage", realPath);
            //InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    public void fingRealPath () {

    }

    public void addImageToGallery(final String filePath, final Context context) {

    ContentValues values = new ContentValues();

    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
    values.put(Images.Media.MIME_TYPE, "image/jpeg");
    values.put(MediaStore.MediaColumns.DATA, filePath);

    context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
}
 
}

